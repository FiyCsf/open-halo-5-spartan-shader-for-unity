// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Open Halo 5 Spartan Shader V1.3"
{
	Properties
	{
		_MainTex("Diffuse", 2D) = "white" {}
		_ControlMap("ControlMap", 2D) = "white" {}
		[Normal]_BumpMap("NormalMap", 2D) = "bump" {}
		[Toggle]_Halo5NormalMap("Halo 5 Normal Map?", Float) = 0
		_MetallicMultiplier("Metallic Multiplier", Range( 0 , 2)) = 1
		_SmoothnessMultiplier("Smoothness Multiplier ", Range( 0 , 2)) = 1
		[HDR]_ColourRedChannel("Colour Red Channel", Color) = (1,0,0,0)
		[HDR]_ColourGreenChannel("Colour Green Channel", Color) = (0.07799447,1,0,0)
		_ADDEmissionTexture("[ADD] Emission Texture:", 2D) = "black" {}
		[HDR]_EmissionColor("Emission Color", Color) = (0,0,0,0)
		[Toggle]_ADDFakeZone3InverseofRGChannels("[ADD] Fake Zone 3? [Inverse of R&G Channels]", Float) = 0
		_FakeZone3Colour("Fake Zone 3 Colour", Color) = (1,1,1,0)
		[Toggle]_IsthisaHelmet("[Is this a Helmet?]", Float) = 1
		[HDR]_ColourOfVisor("Colour Of Visor:", Color) = (1,0.7655172,0,0)
		_VisorMetallicMultiplier("Visor Metallic Multiplier", Range( 0 , 1.25)) = 1
		_VisorSmoothnessMultiplier("Visor Smoothness Multiplier", Range( 0 , 1.25)) = 1
		[Toggle]_ADDVisorDetailAlbedo("[ADD] Visor Detail Albedo?", Float) = 0
		_VisorDetailAlbedo("Visor Detail Albedo", 2D) = "white" {}
		[Toggle]_AmbientOcclusionSetValue("Ambient Occlusion Set Value?", Float) = 1
		_AOSetValue("AO Set Value:", Range( 0.33 , 1)) = 0.5
		[Toggle]_ADDRedChannelDetail("[ADD] Red Channel Detail?", Float) = 1
		_RedDetailTexture("Red Detail Texture:", 2D) = "white" {}
		[Toggle]_ADDGreenChannelDetail("[ADD] Green Channel Detail?", Float) = 0
		_GreenDetailTexture("Green Detail Texture:", 2D) = "white" {}
		[Toggle]_ADDFakeZone3Detail("[ADD] Fake Zone 3 Detail?", Float) = 0
		_Fakezone3DetailTexture("Fake zone 3  Detail Texture:", 2D) = "white" {}
		[Toggle]_ADDPointDevicesScratchEffect("[ADD] PointDevice's Scratch Effect?", Float) = 0
		_ScratchAmount("Scratch Amount", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _Halo5NormalMap;
		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;
		uniform float _ADDPointDevicesScratchEffect;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _IsthisaHelmet;
		uniform sampler2D _ControlMap;
		uniform float4 _ControlMap_ST;
		uniform float _ADDRedChannelDetail;
		uniform float4 _ColourRedChannel;
		uniform sampler2D _RedDetailTexture;
		uniform float4 _RedDetailTexture_ST;
		uniform float _ADDGreenChannelDetail;
		uniform float4 _ColourGreenChannel;
		uniform sampler2D _GreenDetailTexture;
		uniform float4 _GreenDetailTexture_ST;
		uniform float _ADDFakeZone3InverseofRGChannels;
		uniform float _ADDFakeZone3Detail;
		uniform float4 _FakeZone3Colour;
		uniform sampler2D _Fakezone3DetailTexture;
		uniform float4 _Fakezone3DetailTexture_ST;
		uniform float _ADDVisorDetailAlbedo;
		uniform float4 _ColourOfVisor;
		uniform sampler2D _VisorDetailAlbedo;
		uniform float4 _VisorDetailAlbedo_ST;
		uniform float _ScratchAmount;
		uniform sampler2D _ADDEmissionTexture;
		uniform float4 _ADDEmissionTexture_ST;
		uniform float4 _EmissionColor;
		uniform float _MetallicMultiplier;
		uniform float _VisorMetallicMultiplier;
		uniform float _SmoothnessMultiplier;
		uniform float _VisorSmoothnessMultiplier;
		uniform float _AmbientOcclusionSetValue;
		uniform float _AOSetValue;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_BumpMap = i.uv_texcoord * _BumpMap_ST.xy + _BumpMap_ST.zw;
			float3 tex2DNode7 = UnpackNormal( tex2D( _BumpMap, uv_BumpMap ) );
			float3 appendResult14 = (float3(tex2DNode7.r , -tex2DNode7.g , tex2DNode7.b));
			o.Normal = lerp(tex2DNode7,appendResult14,_Halo5NormalMap);
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 Diffuse68 = tex2D( _MainTex, uv_MainTex );
			float2 uv_ControlMap = i.uv_texcoord * _ControlMap_ST.xy + _ControlMap_ST.zw;
			float4 tex2DNode1 = tex2D( _ControlMap, uv_ControlMap );
			float ControlR67 = tex2DNode1.r;
			float2 uv_RedDetailTexture = i.uv_texcoord * _RedDetailTexture_ST.xy + _RedDetailTexture_ST.zw;
			float4 colorZone1Detail166 = tex2D( _RedDetailTexture, uv_RedDetailTexture );
			float ControlG73 = tex2DNode1.g;
			float2 uv_GreenDetailTexture = i.uv_texcoord * _GreenDetailTexture_ST.xy + _GreenDetailTexture_ST.zw;
			float4 ColorZone2Detail168 = tex2D( _GreenDetailTexture, uv_GreenDetailTexture );
			float3 linearToGamma527 = LinearToGammaSpace( tex2DNode1.rgb );
			float3 break528 = linearToGamma527;
			float ControlRgamma535 = break528.x;
			float ControlGgamma536 = break528.y;
			float clampResult533 = clamp( ( ControlRgamma535 + ControlGgamma536 ) , 0.0 , 1.0 );
			float smoothstepResult534 = smoothstep( 0.0 , 1.0 , ( 1.0 - clampResult533 ));
			float4 temp_cast_2 = (smoothstepResult534).xxxx;
			float2 uv_Fakezone3DetailTexture = i.uv_texcoord * _Fakezone3DetailTexture_ST.xy + _Fakezone3DetailTexture_ST.zw;
			float4 ColorZone3Detail170 = tex2D( _Fakezone3DetailTexture, uv_Fakezone3DetailTexture );
			float4 temp_output_205_0 = ( ( ( ControlR67 * lerp(_ColourRedChannel,( _ColourRedChannel * colorZone1Detail166 ),_ADDRedChannelDetail) ) + ( ControlG73 * lerp(_ColourGreenChannel,( _ColourGreenChannel * ColorZone2Detail168 ),_ADDGreenChannelDetail) ) ) + lerp(temp_cast_2,( smoothstepResult534 * lerp(_FakeZone3Colour,ColorZone3Detail170,_ADDFakeZone3Detail) ),_ADDFakeZone3InverseofRGChannels) );
			float clampResult568 = clamp( ( ( ( ControlR67 + ControlG73 ) - 1.2 ) * 100.0 ) , 0.0 , 1.0 );
			float VisorMask293 = clampResult568;
			float2 uv_VisorDetailAlbedo = i.uv_texcoord * _VisorDetailAlbedo_ST.xy + _VisorDetailAlbedo_ST.zw;
			float4 VisorDetail331 = tex2D( _VisorDetailAlbedo, uv_VisorDetailAlbedo );
			float4 VisorColour342 = ( VisorMask293 * lerp(_ColourOfVisor,( _ColourOfVisor * VisorDetail331 ),_ADDVisorDetailAlbedo) );
			float4 temp_cast_3 = (smoothstepResult534).xxxx;
			float4 ifLocalVar515 = 0;
			if( lerp(0.0,1.0,_IsthisaHelmet) == 1.0 )
				ifLocalVar515 = ( ( temp_output_205_0 * ( 1.0 - VisorMask293 ) ) + VisorColour342 );
			else if( lerp(0.0,1.0,_IsthisaHelmet) < 1.0 )
				ifLocalVar515 = temp_output_205_0;
			float4 FiysColourMethod97 = ( Diffuse68 * ifLocalVar515 );
			float3 temp_cast_5 = (_ScratchAmount).xxx;
			float ControlAlpha123 = tex2DNode1.a;
			float temp_output_9_0_g16 = ( 1.0 - ( 0.0 + ( ( ControlAlpha123 + 0.5 ) * 1.0 ) ) );
			float temp_output_18_0_g16 = ( 1.0 - temp_output_9_0_g16 );
			float3 appendResult16_g16 = (float3(temp_output_18_0_g16 , temp_output_18_0_g16 , temp_output_18_0_g16));
			o.Albedo = lerp(FiysColourMethod97,float4( ( FiysColourMethod97.rgb * ( ( ( temp_cast_5 * (unity_ColorSpaceDouble).rgb ) * temp_output_9_0_g16 ) + appendResult16_g16 ) ) , 0.0 ),_ADDPointDevicesScratchEffect).rgb;
			float2 uv_ADDEmissionTexture = i.uv_texcoord * _ADDEmissionTexture_ST.xy + _ADDEmissionTexture_ST.zw;
			o.Emission = ( tex2D( _ADDEmissionTexture, uv_ADDEmissionTexture ) * _EmissionColor ).rgb;
			float ControlBlue121 = tex2DNode1.b;
			float temp_output_100_0 = ( ControlBlue121 * _MetallicMultiplier );
			float temp_output_381_0 = ( 1.0 - VisorMask293 );
			float ifLocalVar490 = 0;
			if( lerp(0.0,1.0,_IsthisaHelmet) == 1.0 )
				ifLocalVar490 = ( ( temp_output_100_0 * temp_output_381_0 ) + ( ( ControlAlpha123 * VisorMask293 ) * _VisorMetallicMultiplier ) );
			else if( lerp(0.0,1.0,_IsthisaHelmet) < 1.0 )
				ifLocalVar490 = temp_output_100_0;
			float VarMetallic428 = ifLocalVar490;
			o.Metallic = VarMetallic428;
			float temp_output_21_0 = ( ControlAlpha123 * _SmoothnessMultiplier );
			float ifLocalVar491 = 0;
			if( lerp(0.0,1.0,_IsthisaHelmet) == 1.0 )
				ifLocalVar491 = ( ( temp_output_21_0 * temp_output_381_0 ) + ( ( ControlR67 * VisorMask293 ) * _VisorSmoothnessMultiplier ) );
			else if( lerp(0.0,1.0,_IsthisaHelmet) < 1.0 )
				ifLocalVar491 = temp_output_21_0;
			float VarSmoothness429 = ifLocalVar491;
			o.Smoothness = VarSmoothness429;
			o.Occlusion = lerp(tex2DNode7.b,_AOSetValue,_AmbientOcclusionSetValue);
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Standard"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
7;29;2546;1364;4219.023;2065.607;4.792272;True;False
Node;AmplifyShaderEditor.CommentaryNode;57;-512,-1408;Float;False;4860.504;2602.578;Colour Zones Metallics and Smoothness are all contained within the Control File. Red is Colour Zone 1, Green 2, Blue is the Halo 5 Metallic, And Alpha is the Halo 5 Smoothness;11;68;114;6;1;125;65;226;527;528;535;536;Diffuse & Control: Your Colouring Metallic and Smoothness Data;0,0.3379312,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;226;816,-1344;Float;False;664.4159;777.4517;Registering Detail Albedos;8;331;332;170;171;168;167;166;165;Detail Albedos;0.9926471,0.175173,0.93627,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;65;-480,-544;Float;False;3888.716;1682.24;Using the Control, and Defuse, Let's Get some Armor Colours Going                                 More subtle then inital method, prevents blown colours.;31;126;128;380;348;346;344;213;211;347;205;343;82;81;64;116;488;489;515;542;543;544;545;546;547;548;549;550;551;552;553;558;Fiy's Colouring Method, with Point's Detail textures;0.3529412,0.8929005,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;126;192,672;Float;False;3113.492;423.854;Here the Red and Green Of the Control are Added, and then inverted, to create us the 3rd zone of the Armor's Colouring;21;94;96;208;195;177;192;206;194;193;199;120;174;207;443;196;197;198;85;526;533;534;THERE IS NO DEFAULT 3rd COLOUR ZONE;1,0.8482759,0,1;0;0
Node;AmplifyShaderEditor.SamplerNode;171;848,-960;Float;True;Property;_Fakezone3DetailTexture;Fake zone 3  Detail Texture:;25;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;170;1184,-960;Float;True;ColorZone3Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;120;224,720;Float;False;Property;_FakeZone3Colour;Fake Zone 3 Colour;11;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;174;208,896;Float;True;170;ColorZone3Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;443;464,960;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;177;496,992;Float;False;Property;_ADDFakeZone3Detail;[ADD] Fake Zone 3 Detail?;24;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;199;720,1040;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;1;-496,-1168;Float;True;Property;_ControlMap;ControlMap;1;0;Create;True;0;0;False;0;None;dcd0ad11607c700439058b69d34ac333;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;197;736,1040;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LinearToGammaNode;527;-496,-960;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;198;752,1040;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;528;-496,-880;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.WireNode;196;2544,1040;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;536;-272,-784;Float;True;ControlGgamma;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;535;-272,-960;Float;True;ControlRgamma;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;192;2560,1040;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;208;512,720;Float;True;535;ControlRgamma;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;736,800;Float;True;536;ControlGgamma;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;85;960,720;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;195;2560,1040;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;194;2560,944;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;533;1168,720;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;167;848,-1136;Float;True;Property;_GreenDetailTexture;Green Detail Texture:;23;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;165;848,-1312;Float;True;Property;_RedDetailTexture;Red Detail Texture:;21;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;193;2560,944;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;168;1184,-1136;Float;True;ColorZone2Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;526;1424,720;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;166;1184,-1312;Float;True;colorZone1Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;116;-464,-496;Float;False;618.8059;1581.453;Grabbing Data From Split & User Etc;11;172;79;119;173;175;176;66;118;69;209;210;;1,0,0,1;0;0
Node;AmplifyShaderEditor.ColorNode;119;-448,512;Float;False;Property;_ColourGreenChannel;Colour Green Channel;7;1;[HDR];Create;True;0;0;False;0;0.07799447,1,0,0;0,1,0.08965511,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;172;-448,112;Float;True;166;colorZone1Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;118;-448,-64;Float;False;Property;_ColourRedChannel;Colour Red Channel;6;1;[HDR];Create;True;0;0;False;0;1,0,0,0;1,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;173;-448,688;Float;True;168;ColorZone2Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;206;2576,944;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;125;224,-1312;Float;False;528.7554;750.0869;Spliting the Control into channels as intended for use;6;67;280;279;121;123;73;The Control Split;1,1,1,1;0;0
Node;AmplifyShaderEditor.SmoothstepOpNode;534;1600,719;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;2720,832;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;209;-192,96;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;210;-192,672;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;528,-1024;Float;True;ControlG;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;67;336,-1264;Float;True;ControlR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;176;-128,-64;Float;False;Property;_ADDRedChannelDetail;[ADD] Red Channel Detail?;20;0;Create;True;0;0;False;0;1;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;175;-128,513.3;Float;False;Property;_ADDGreenChannelDetail;[ADD] Green Channel Detail?;22;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-449.3,304;Float;True;73;ControlG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;294;-507.8409,1227.841;Float;False;2748.289;1140.181;Fiy's Visor Isolation Method, Crude, but it works;18;293;334;337;338;336;339;257;256;422;423;414;415;568;567;566;560;340;342;Visor;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;66;-448,-258;Float;True;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;96;3008,752;Float;True;Property;_ADDFakeZone3InverseofRGChannels;[ADD] Fake Zone 3? [Inverse of R&G Channels];10;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;176,-113;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;56;-496,2992;Float;False;1784.802;516.9932;For Normal Maps, *NOTE* that the default format of Halo Normal Maps is for Direct X. Unity uses OPENGL Rendering, where the Green channel is used differently. INVERT GREEN CHANNEL TO FIX IT FOR UNITY!;24;11;14;7;138;214;215;216;217;218;219;220;221;222;223;224;225;247;248;250;251;252;301;302;511;Normals;0,0.6689658,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;256;-475.8409,1307.841;Float;True;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;257;-475.8409,1499.841;Float;True;73;ControlG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;489;3280,592;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;192,384;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;82;416,176;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;488;752,496;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;7;-464,3040;Float;True;Property;_BumpMap;NormalMap;2;1;[Normal];Create;False;0;0;False;0;None;1af08bf3ba4463343a45d70720aa6a9d;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;560;-144,1376;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;205;896,272;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;250;-176,3184;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;566;80,1376;Float;True;2;0;FLOAT;0;False;1;FLOAT;1.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;332;848,-784;Float;True;Property;_VisorDetailAlbedo;Visor Detail Albedo;17;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;216;-80,3168;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;545;1104,64;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;222;-64,3344;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;248;-176,3456;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;567;320,1376;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;100;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;331;1184,-784;Float;True;VisorDetail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;336;-475.8409,2091.841;Float;True;331;VisorDetail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;339;-475.8409,1914.057;Float;False;Property;_ColourOfVisor;Colour Of Visor:;13;1;[HDR];Create;True;0;0;False;0;1,0.7655172,0,0;1,0.7241379,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;214;-80,3184;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;220;-64,3360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;247;-160,3456;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;568;640,1376;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;546;1104,48;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;223;-64,3360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;215;-64,3184;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;552;1264,304;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;114;1504,-1344;Float;False;2810.784;765.5772;Hidden in the Control, and tossed by most, the Blue channel is actually the Metallic, and Alpha is Smoothness;32;428;429;385;400;383;389;384;398;397;21;399;393;381;100;388;99;20;124;394;404;122;396;382;405;406;408;407;409;490;491;386;506;Metallic And Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;293;896,1376;Float;True;VisorMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;553;1088,48;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;338;-219.841,2075.841;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;251;1232,3456;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;138;-16,3152;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;337;-155.841,1915.841;Float;False;Property;_ADDVisorDetailAlbedo;[ADD] Visor Detail Albedo?;16;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;6;-496,-1360;Float;True;Property;_MainTex;Diffuse;0;0;Create;False;0;0;False;0;None;c15ae552b4ebae54eb8f4c983eb73cc0;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;386;2912,-992;Float;False;293;VisorMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;252;1248,3456;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;320;4144,1536;Float;False;1035.154;830.4284;Where it's all coming Together;22;0;315;316;310;309;308;307;202;243;299;300;321;322;323;324;325;430;431;432;433;507;508;OUTPUT;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;334;-475.8409,1723.841;Float;True;293;VisorMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;550;1280,304;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;544;992,48;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;511;-48,3360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;343;640,-208;Float;True;293;VisorMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;279;240,-752;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;542;976,48;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;340;272,1728;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;224;128,3360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;551;1280,288;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;0,-1360;Float;False;Diffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;409;3152,-960;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;299;4160,2128;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;217;128,3184;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;218;144,3184;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;-448,-464;Float;True;68;Diffuse;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;407;3168,-960;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;342;560,1728;Float;True;VisorColour;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;347;832,-208;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;549;1344,128;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;300;4176,2128;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;543;976,32;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;280;256,-752;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;221;144,3360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;202;4160,2224;Float;False;Property;_AOSetValue;AO Set Value:;19;0;Create;True;0;0;False;0;0.5;0;0.33;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;211;1552,-416;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;225;144,3344;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;219;144,3168;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;58;-496,2480;Float;False;1868.273;393.552;Add's a Scratch-Like Effect based on the Control maps details;8;54;55;98;48;51;41;47;127;PointDevice's Wear Effects;1,0.6827586,0,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;346;1056,-240;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;123;528,-784;Float;True;ControlAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;408;3168,-944;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;344;1056,-32;Float;False;342;VisorColour;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;548;1344,112;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;243;4450,2089;Float;True;Property;_AmbientOcclusionSetValue;Ambient Occlusion Set Value?;18;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;506;3504,-960;Float;False;Property;_IsthisaHelmet;[Is this a Helmet?];12;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;127;-464,2528;Float;True;123;ControlAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;323;4720,2112;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;406;3168,-688;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;14;272,3104;Float;True;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;547;1360,112;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;348;1280,-144;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;213;1568,-400;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;121;272,-960;Float;True;ControlBlue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;322;4720,2096;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;380;1584,-384;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;405;3168,-672;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;47;-224,2528;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;515;1520,-16;Float;True;False;5;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;11;751,3040;Float;True;Property;_Halo5NormalMap;Halo 5 Normal Map?;3;0;Create;True;0;0;False;0;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;394;3040,-1136;Float;False;123;ControlAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;122;1520,-1264;Float;True;121;ControlBlue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;124;1520,-880;Float;True;123;ControlAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;558;2400,-32;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;382;2496,-976;Float;False;293;VisorMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;180;-496,3632;Float;False;1789.95;466.3504;Maps for lighting on the Armor;5;144;143;131;509;510;Emmission;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.WireNode;302;1200,3072;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;99;1728,-1200;Float;False;Property;_MetallicMultiplier;Metallic Multiplier;4;0;Create;True;0;0;False;0;1;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;0,2528;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;1728,-800;Float;False;Property;_SmoothnessMultiplier;Smoothness Multiplier ;5;0;Create;True;0;0;False;0;1;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;396;3024,-752;Float;False;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;128;2623.3,-83.00001;Float;False;478.1267;151.4682;Export Finished Colouring For Use Elsewhere in the Shader;1;97;;0,1,0.2551723,1;0;0
Node;AmplifyShaderEditor.WireNode;404;3184,-672;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;431;4720,2032;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;131;-464,3696;Float;True;Property;_ADDEmissionTexture;[ADD] Emission Texture:;8;0;Create;True;0;0;False;0;None;225256eeb9fc5df4f81ed714750a0fa3;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;388;3232,-1040;Float;False;Property;_VisorMetallicMultiplier;Visor Metallic Multiplier;14;0;Create;True;0;0;False;0;1;1;0;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;397;3200,-752;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;399;3216,-656;Float;False;Property;_VisorSmoothnessMultiplier;Visor Smoothness Multiplier;15;0;Create;True;0;0;False;0;1;1;0;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;224,2528;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;430;4720,2016;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;143;-384,3888;Float;False;Property;_EmissionColor;Emission Color;9;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,1.42069,2,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;301;1216,3072;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;393;3232,-1136;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;381;2688,-976;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;2000,-880;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;97;2751.3,-35;Float;False;FiysColourMethod;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;2000,-1264;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;389;3360,-1136;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;55;544,2768;Float;False;Property;_ScratchAmount;Scratch Amount;27;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;304;1728,3072;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;325;4640,2000;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;398;3328,-752;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;144;-64,3728;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;384;2832,-816;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;98;640,2528;Float;False;97;FiysColourMethod;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;48;448,2528;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;383;2832,-1200;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;117;1392,2480;Float;False;97;FiysColourMethod;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;400;3520,-816;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;510;1216,3760;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;385;3520,-1200;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;54;896,2528;Float;True;Detail Albedo;28;;16;29e5a290b15a7884983e27c8f1afaa8c;0;3;12;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;9;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;324;4640,1984;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;303;1744,3072;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ToggleSwitchNode;44;1664,2480;Float;True;Property;_ADDPointDevicesScratchEffect;[ADD] PointDevice's Scratch Effect?;26;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;509;1232,3760;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ConditionalIfNode;490;3856,-1280;Float;True;False;5;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;305;1760,3072;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;321;4640,1760;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;491;3855,-896;Float;True;False;5;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;507;4352,1664;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;309;4352,1632;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;316;4640,1744;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;429;4096,-896;Float;False;VarSmoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;428;4112,-1280;Float;False;VarMetallic;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;308;4352,1600;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;423;1488,1856;Float;False;Clamp1VAR;-1;True;1;0;INT;0;False;1;INT;0
Node;AmplifyShaderEditor.IntNode;414;1296,1792;Float;False;Constant;_Clamp0Value;Clamp 0 Value;30;0;Create;True;0;0;False;0;0;0;0;1;INT;0
Node;AmplifyShaderEditor.WireNode;310;4368,1632;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;307;4368,1600;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;433;4336,1760;Float;False;429;VarSmoothness;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;432;4368,1696;Float;False;428;VarMetallic;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;315;4672,1728;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;415;1296,1856;Float;False;Constant;_Clamp1Value;Clamp 1 Value;30;0;Create;True;0;0;False;0;1;0;0;1;INT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;422;1488,1792;Float;False;Clamp0VAR;-1;True;1;0;INT;0;False;1;INT;0
Node;AmplifyShaderEditor.WireNode;508;4368,1664;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;4688,1584;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Open Halo 5 Spartan Shader V1.3;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;Standard;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;129;-512,-1792;Float;False;100;100;...;0;Created By PointDevice And FiyCsf For Use With Halo 5 Spartan Models;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;253;-512,-1632;Float;False;361.9877;100;GNU LESSER GENERAL PUBLIC LICENSE                        Version 2.1, February 1999   Copyright (C) 1991, 1999 Free Software Foundation, Inc.  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  Everyone is permitted to copy and distribute verbatim copies  of this license document, but changing it is not allowed.  (This is the first released version of the Lesser GPL.  It also counts  as the successor of the GNU Library Public License, version 2, hence  the version number 2.1.)                              Preamble    The licenses for most software are designed to take away your freedom to share and change it.  By contrast, the GNU General Public Licenses are intended to guarantee your freedom to share and change free software--to make sure the software is free for all its users.    This license, the Lesser General Public License, applies to some specially designated software packages--typically libraries--of the Free Software Foundation and other authors who decide to use it.  You can use it too, but we suggest you first think carefully about whether this license or the ordinary General Public License is the better strategy to use in any particular case, based on the explanations below.    When we speak of free software, we are referring to freedom of use, not price.  Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for this service if you wish)  that you receive source code or can get it if you want it  that you can change the software and use pieces of it in new free programs  and that you are informed that you can do these things.    To protect your rights, we need to make restrictions that forbid distributors to deny you these rights or to ask you to surrender these rights.  These restrictions translate to certain responsibilities for you if you distribute copies of the library or if you modify it.    For example, if you distribute copies of the library, whether gratis or for a fee, you must give the recipients all the rights that we gave you.  You must make sure that they, too, receive or can get the source code.  If you link other code with the library, you must provide complete object files to the recipients, so that they can relink them with the library after making changes to the library and recompiling it.  And you must show them these terms so they know their rights.    We protect your rights with a two-step method: (1) we copyright the library, and (2) we offer you this license, which gives you legal permission to copy, distribute and/or modify the library.    To protect each distributor, we want to make it very clear that there is no warranty for the free library.  Also, if the library is modified by someone else and passed on, the recipients should know that what they have is not the original version, so that the original author's reputation will not be affected by problems that might be introduced by others.    Finally, software patents pose a constant threat to the existence of any free program.  We wish to make sure that a company cannot effectively restrict the users of a free program by obtaining a restrictive license from a patent holder.  Therefore, we insist that any patent license obtained for a version of the library must be consistent with the full freedom of use specified in this license.    Most GNU software, including some libraries, is covered by the ordinary GNU General Public License.  This license, the GNU Lesser General Public License, applies to certain designated libraries, and is quite different from the ordinary General Public License.  We use this license for certain libraries in order to permit linking those libraries into non-free programs.    When a program is linked with a library, whether statically or using a shared library, the combination of the two is legally speaking a combined work, a derivative of the original library.  The ordinary General Public License therefore permits such linking only if the entire combination fits its criteria of freedom.  The Lesser General Public License permits more lax criteria for linking other code with the library.    We call this license the "Lesser" General Public License because it does Less to protect the user's freedom than the ordinary General Public License.  It also provides other free software developers Less of an advantage over competing non-free programs.  These disadvantages are the reason we use the ordinary General Public License for many libraries.  However, the Lesser license provides advantages in certain special circumstances.    For example, on rare occasions, there may be a special need to encourage the widest possible use of a certain library, so that it becomes a de-facto standard.  To achieve this, non-free programs must be allowed to use the library.  A more frequent case is that a free library does the same job as widely used non-free libraries.  In this case, there is little to gain by limiting the free library to free software only, so we use the Lesser General Public License.    In other cases, permission to use a particular library in non-free programs enables a greater number of people to use a large body of free software.  For example, permission to use the GNU C Library in non-free programs enables many more people to use the whole GNU operating system, as well as its variant, the GNU/Linux operating system.    Although the Lesser General Public License is Less protective of the users' freedom, it does ensure that the user of a program that is linked with the Library has the freedom and the wherewithal to run that program using a modified version of the Library.    The precise terms and conditions for copying, distribution and modification follow.  Pay close attention to the difference between a "work based on the library" and a "work that uses the library".  The former contains code derived from the library, whereas the latter must be combined with the library in order to run.                    GNU LESSER GENERAL PUBLIC LICENSE    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION    0. This License Agreement applies to any software library or other program which contains a notice placed by the copyright holder or other authorized party saying it may be distributed under the terms of this Lesser General Public License (also called "this License"). Each licensee is addressed as "you".    A "library" means a collection of software functions and/or data prepared so as to be conveniently linked with application programs (which use some of those functions and data) to form executables.    The "Library", below, refers to any such software library or work which has been distributed under these terms.  A "work based on the Library" means either the Library or any derivative work under copyright law: that is to say, a work containing the Library or a portion of it, either verbatim or with modifications and/or translated straightforwardly into another language.  (Hereinafter, translation is included without limitation in the term "modification".)    "Source code" for a work means the preferred form of the work for making modifications to it.  For a library, complete source code means all the source code for all modules it contains, plus any associated interface definition files, plus the scripts used to control compilation and installation of the library.    Activities other than copying, distribution and modification are not covered by this License  they are outside its scope.  The act of running a program using the Library is not restricted, and output from such a program is covered only if its contents constitute a work based on the Library (independent of the use of the Library in a tool for writing it).  Whether that is true depends on what the Library does and what the program that uses the Library does.    1. You may copy and distribute verbatim copies of the Library's complete source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice and disclaimer of warranty  keep intact all the notices that refer to this License and to the absence of any warranty  and distribute a copy of this License along with the Library.    You may charge a fee for the physical act of transferring a copy, and you may at your option offer warranty protection in exchange for a fee.    2. You may modify your copy or copies of the Library or any portion of it, thus forming a work based on the Library, and copy and distribute such modifications or work under the terms of Section 1 above, provided that you also meet all of these conditions:      a) The modified work must itself be a software library.      b) You must cause the files modified to carry prominent notices     stating that you changed the files and the date of any change.      c) You must cause the whole of the work to be licensed at no     charge to all third parties under the terms of this License.      d) If a facility in the modified Library refers to a function or a     table of data to be supplied by an application program that uses     the facility, other than as an argument passed when the facility     is invoked, then you must make a good faith effort to ensure that,     in the event an application does not supply such function or     table, the facility still operates, and performs whatever part of     its purpose remains meaningful.      (For example, a function in a library to compute square roots has     a purpose that is entirely well-defined independent of the     application.  Therefore, Subsection 2d requires that any     application-supplied function or table used by this function must     be optional: if the application does not supply it, the square     root function must still compute square roots.)  These requirements apply to the modified work as a whole.  If identifiable sections of that work are not derived from the Library, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works.  But when you distribute the same sections as part of a whole which is a work based on the Library, the distribution of the whole must be on the terms of this License, whose permissions for other licensees extend to the entire whole, and thus to each and every part regardless of who wrote it.  Thus, it is not the intent of this section to claim rights or contest your rights to work written entirely by you  rather, the intent is to exercise the right to control the distribution of derivative or collective works based on the Library.  In addition, mere aggregation of another work not based on the Library with the Library (or with a work based on the Library) on a volume of a storage or distribution medium does not bring the other work under the scope of this License.    3. You may opt to apply the terms of the ordinary GNU General Public License instead of this License to a given copy of the Library.  To do this, you must alter all the notices that refer to this License, so that they refer to the ordinary GNU General Public License, version 2, instead of to this License.  (If a newer version than version 2 of the ordinary GNU General Public License has appeared, then you can specify that version instead if you wish.)  Do not make any other change in these notices.    Once this change is made in a given copy, it is irreversible for that copy, so the ordinary GNU General Public License applies to all subsequent copies and derivative works made from that copy.    This option is useful when you wish to copy part of the code of the Library into a program that is not a library.    4. You may copy and distribute the Library (or a portion or derivative of it, under Section 2) in object code or executable form under the terms of Sections 1 and 2 above provided that you accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange.    If distribution of object code is made by offering access to copy from a designated place, then offering equivalent access to copy the source code from the same place satisfies the requirement to distribute the source code, even though third parties are not compelled to copy the source along with the object code.    5. A program that contains no derivative of any portion of the Library, but is designed to work with the Library by being compiled or linked with it, is called a "work that uses the Library".  Such a work, in isolation, is not a derivative work of the Library, and therefore falls outside the scope of this License.    However, linking a "work that uses the Library" with the Library creates an executable that is a derivative of the Library (because it contains portions of the Library), rather than a "work that uses the library".  The executable is therefore covered by this License. Section 6 states terms for distribution of such executables.    When a "work that uses the Library" uses material from a header file that is part of the Library, the object code for the work may be a derivative work of the Library even though the source code is not. Whether this is true is especially significant if the work can be linked without the Library, or if the work is itself a library.  The threshold for this to be true is not precisely defined by law.    If such an object file uses only numerical parameters, data structure layouts and accessors, and small macros and small inline functions (ten lines or less in length), then the use of the object file is unrestricted, regardless of whether it is legally a derivative work.  (Executables containing this object code plus portions of the Library will still fall under Section 6.)    Otherwise, if the work is a derivative of the Library, you may distribute the object code for the work under the terms of Section 6. Any executables containing that work also fall under Section 6, whether or not they are linked directly with the Library itself.    6. As an exception to the Sections above, you may also combine or link a "work that uses the Library" with the Library to produce a work containing portions of the Library, and distribute that work under terms of your choice, provided that the terms permit modification of the work for the customer's own use and reverse engineering for debugging such modifications.    You must give prominent notice with each copy of the work that the Library is used in it and that the Library and its use are covered by this License.  You must supply a copy of this License.  If the work during execution displays copyright notices, you must include the copyright notice for the Library among them, as well as a reference directing the user to the copy of this License.  Also, you must do one of these things:      a) Accompany the work with the complete corresponding     machine-readable source code for the Library including whatever     changes were used in the work (which must be distributed under     Sections 1 and 2 above)  and, if the work is an executable linked     with the Library, with the complete machine-readable "work that     uses the Library", as object code and/or source code, so that the     user can modify the Library and then relink to produce a modified     executable containing the modified Library.  (It is understood     that the user who changes the contents of definitions files in the     Library will not necessarily be able to recompile the application     to use the modified definitions.)      b) Use a suitable shared library mechanism for linking with the     Library.  A suitable mechanism is one that (1) uses at run time a     copy of the library already present on the user's computer system,     rather than copying library functions into the executable, and (2)     will operate properly with a modified version of the library, if     the user installs one, as long as the modified version is     interface-compatible with the version that the work was made with.      c) Accompany the work with a written offer, valid for at     least three years, to give the same user the materials     specified in Subsection 6a, above, for a charge no more     than the cost of performing this distribution.      d) If distribution of the work is made by offering access to copy     from a designated place, offer equivalent access to copy the above     specified materials from the same place.      e) Verify that the user has already received a copy of these     materials or that you have already sent this user a copy.    For an executable, the required form of the "work that uses the Library" must include any data and utility programs needed for reproducing the executable from it.  However, as a special exception, the materials to be distributed need not include anything that is normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which the executable runs, unless that component itself accompanies the executable.    It may happen that this requirement contradicts the license restrictions of other proprietary libraries that do not normally accompany the operating system.  Such a contradiction means you cannot use both them and the Library together in an executable that you distribute.    7. You may place library facilities that are a work based on the Library side-by-side in a single library together with other library facilities not covered by this License, and distribute such a combined library, provided that the separate distribution of the work based on the Library and of the other library facilities is otherwise permitted, and provided that you do these two things:      a) Accompany the combined library with a copy of the same work     based on the Library, uncombined with any other library     facilities.  This must be distributed under the terms of the     Sections above.      b) Give prominent notice with the combined library of the fact     that part of it is a work based on the Library, and explaining     where to find the accompanying uncombined form of the same work.    8. You may not copy, modify, sublicense, link with, or distribute the Library except as expressly provided under this License.  Any attempt otherwise to copy, modify, sublicense, link with, or distribute the Library is void, and will automatically terminate your rights under this License.  However, parties who have received copies, or rights, from you under this License will not have their licenses terminated so long as such parties remain in full compliance.    9. You are not required to accept this License, since you have not signed it.  However, nothing else grants you permission to modify or distribute the Library or its derivative works.  These actions are prohibited by law if you do not accept this License.  Therefore, by modifying or distributing the Library (or any work based on the Library), you indicate your acceptance of this License to do so, and all its terms and conditions for copying, distributing or modifying the Library or works based on it.    10. Each time you redistribute the Library (or any work based on the Library), the recipient automatically receives a license from the original licensor to copy, distribute, link with or modify the Library subject to these terms and conditions.  You may not impose any further restrictions on the recipients' exercise of the rights granted herein. You are not responsible for enforcing compliance by third parties with this License.    11. If, as a consequence of a court judgment or allegation of patent infringement or for any other reason (not limited to patent issues), conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License.  If you cannot distribute so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not distribute the Library at all.  For example, if a patent license would not permit royalty-free redistribution of the Library by all those who receive copies directly or indirectly through you, then the only way you could satisfy both it and this License would be to refrain entirely from distribution of the Library.  If any portion of this section is held invalid or unenforceable under any particular circumstance, the balance of the section is intended to apply, and the section as a whole is intended to apply in other circumstances.  It is not the purpose of this section to induce you to infringe any patents or other property right claims or to contest validity of any such claims  this section has the sole purpose of protecting the integrity of the free software distribution system which is implemented by public license practices.  Many people have made generous contributions to the wide range of software distributed through that system in reliance on consistent application of that system  it is up to the author/donor to decide if he or she is willing to distribute software through any other system and a licensee cannot impose that choice.  This section is intended to make thoroughly clear what is believed to be a consequence of the rest of this License.    12. If the distribution and/or use of the Library is restricted in certain countries either by patents or by copyrighted interfaces, the original copyright holder who places the Library under this License may add an explicit geographical distribution limitation excluding those countries, so that distribution is permitted only in or among countries not thus excluded.  In such case, this License incorporates the limitation as if written in the body of this License.    13. The Free Software Foundation may publish revised and/or new versions of the Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.  Each version is given a distinguishing version number.  If the Library specifies a version number of this License which applies to it and "any later version", you have the option of following the terms and conditions either of that version or of any later version published by the Free Software Foundation.  If the Library does not specify a license version number, you may choose any version ever published by the Free Software Foundation.    14. If you wish to incorporate parts of the Library into other free programs whose distribution conditions are incompatible with these, write to the author to ask for permission.  For software which is copyrighted by the Free Software Foundation, write to the Free Software Foundation  we sometimes make exceptions for this.  Our decision will be guided by the two goals of preserving the free status of all derivatives of our free software and of promoting the sharing and reuse of software generally.                              NO WARRANTY    15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.    16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.                       END OF TERMS AND CONDITIONS             How to Apply These Terms to Your New Libraries    If you develop a new library, and you want it to be of the greatest possible use to the public, we recommend making it free software that everyone can redistribute and change.  You can do so by permitting redistribution under these terms (or, alternatively, under the terms of the ordinary General Public License).    To apply these terms, attach the following notices to the library.  It is safest to attach them to the start of each source file to most effectively convey the exclusion of warranty  and each file should have at least the "copyright" line and a pointer to where the full notice is found.      shaders     Copyright (C) 2019 John W      This library is free software  you can redistribute it and/or     modify it under the terms of the GNU Lesser General Public     License as published by the Free Software Foundation  either     version 2.1 of the License, or (at your option) any later version.      This library is distributed in the hope that it will be useful,     but WITHOUT ANY WARRANTY  without even the implied warranty of     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     Lesser General Public License for more details.      You should have received a copy of the GNU Lesser General Public     License along with this library  if not, write to the Free Software     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301     USA  Also add information on how to contact you by electronic and paper mail.  You should also get your employer (if you work as a programmer) or your school, if any, to sign a "copyright disclaimer" for the library, if necessary.  Here is a sample  alter the names:    Yoyodyne, Inc., hereby disclaims all copyright interest in the   library `Frob' (a library for tweaking knobs) written by James Random   Hacker.    {signature of Ty Coon}, 1 April 1990   Ty Coon, President of Vice  That's all there is to it! ;0;This Shader is Licensed under the LGPL License Please read the associated Text File.;1,1,1,1;0;0
WireConnection;170;0;171;0
WireConnection;443;0;120;0
WireConnection;177;0;443;0
WireConnection;177;1;174;0
WireConnection;199;0;177;0
WireConnection;197;0;199;0
WireConnection;527;0;1;0
WireConnection;198;0;197;0
WireConnection;528;0;527;0
WireConnection;196;0;198;0
WireConnection;536;0;528;1
WireConnection;535;0;528;0
WireConnection;192;0;196;0
WireConnection;85;0;208;0
WireConnection;85;1;207;0
WireConnection;195;0;192;0
WireConnection;194;0;195;0
WireConnection;533;0;85;0
WireConnection;193;0;194;0
WireConnection;168;0;167;0
WireConnection;526;0;533;0
WireConnection;166;0;165;0
WireConnection;206;0;193;0
WireConnection;534;0;526;0
WireConnection;94;0;534;0
WireConnection;94;1;206;0
WireConnection;209;0;118;0
WireConnection;209;1;172;0
WireConnection;210;0;119;0
WireConnection;210;1;173;0
WireConnection;73;0;1;2
WireConnection;67;0;1;1
WireConnection;176;0;118;0
WireConnection;176;1;209;0
WireConnection;175;0;119;0
WireConnection;175;1;210;0
WireConnection;96;0;534;0
WireConnection;96;1;94;0
WireConnection;64;0;66;0
WireConnection;64;1;176;0
WireConnection;489;0;96;0
WireConnection;81;0;79;0
WireConnection;81;1;175;0
WireConnection;82;0;64;0
WireConnection;82;1;81;0
WireConnection;488;0;489;0
WireConnection;560;0;256;0
WireConnection;560;1;257;0
WireConnection;205;0;82;0
WireConnection;205;1;488;0
WireConnection;250;0;7;3
WireConnection;566;0;560;0
WireConnection;216;0;7;2
WireConnection;545;0;205;0
WireConnection;222;0;7;3
WireConnection;248;0;250;0
WireConnection;567;0;566;0
WireConnection;331;0;332;0
WireConnection;214;0;216;0
WireConnection;220;0;222;0
WireConnection;247;0;248;0
WireConnection;568;0;567;0
WireConnection;546;0;545;0
WireConnection;223;0;220;0
WireConnection;215;0;214;0
WireConnection;552;0;205;0
WireConnection;293;0;568;0
WireConnection;553;0;546;0
WireConnection;338;0;339;0
WireConnection;338;1;336;0
WireConnection;251;0;247;0
WireConnection;138;0;215;0
WireConnection;337;0;339;0
WireConnection;337;1;338;0
WireConnection;252;0;251;0
WireConnection;550;0;552;0
WireConnection;544;0;553;0
WireConnection;511;0;223;0
WireConnection;279;0;1;4
WireConnection;542;0;544;0
WireConnection;340;0;334;0
WireConnection;340;1;337;0
WireConnection;224;0;511;0
WireConnection;551;0;550;0
WireConnection;68;0;6;0
WireConnection;409;0;386;0
WireConnection;299;0;252;0
WireConnection;217;0;138;0
WireConnection;218;0;217;0
WireConnection;407;0;409;0
WireConnection;342;0;340;0
WireConnection;347;0;343;0
WireConnection;549;0;551;0
WireConnection;300;0;299;0
WireConnection;543;0;542;0
WireConnection;280;0;279;0
WireConnection;221;0;224;0
WireConnection;211;0;69;0
WireConnection;225;0;221;0
WireConnection;219;0;218;0
WireConnection;346;0;543;0
WireConnection;346;1;347;0
WireConnection;123;0;280;0
WireConnection;408;0;407;0
WireConnection;548;0;549;0
WireConnection;243;0;300;0
WireConnection;243;1;202;0
WireConnection;323;0;243;0
WireConnection;406;0;408;0
WireConnection;14;0;7;1
WireConnection;14;1;219;0
WireConnection;14;2;225;0
WireConnection;547;0;548;0
WireConnection;348;0;346;0
WireConnection;348;1;344;0
WireConnection;213;0;211;0
WireConnection;121;0;1;3
WireConnection;322;0;323;0
WireConnection;380;0;213;0
WireConnection;405;0;406;0
WireConnection;47;0;127;0
WireConnection;515;0;506;0
WireConnection;515;3;348;0
WireConnection;515;4;547;0
WireConnection;11;0;7;0
WireConnection;11;1;14;0
WireConnection;558;0;380;0
WireConnection;558;1;515;0
WireConnection;302;0;11;0
WireConnection;41;0;47;0
WireConnection;404;0;405;0
WireConnection;431;0;322;0
WireConnection;397;0;396;0
WireConnection;397;1;404;0
WireConnection;51;1;41;0
WireConnection;430;0;431;0
WireConnection;301;0;302;0
WireConnection;393;0;394;0
WireConnection;393;1;386;0
WireConnection;381;0;382;0
WireConnection;21;0;124;0
WireConnection;21;1;20;0
WireConnection;97;0;558;0
WireConnection;100;0;122;0
WireConnection;100;1;99;0
WireConnection;389;0;393;0
WireConnection;389;1;388;0
WireConnection;304;0;301;0
WireConnection;325;0;430;0
WireConnection;398;0;397;0
WireConnection;398;1;399;0
WireConnection;144;0;131;0
WireConnection;144;1;143;0
WireConnection;384;0;21;0
WireConnection;384;1;381;0
WireConnection;48;0;51;0
WireConnection;383;0;100;0
WireConnection;383;1;381;0
WireConnection;400;0;384;0
WireConnection;400;1;398;0
WireConnection;510;0;144;0
WireConnection;385;0;383;0
WireConnection;385;1;389;0
WireConnection;54;12;98;0
WireConnection;54;11;55;0
WireConnection;54;9;48;0
WireConnection;324;0;325;0
WireConnection;303;0;304;0
WireConnection;44;0;117;0
WireConnection;44;1;54;0
WireConnection;509;0;510;0
WireConnection;490;0;506;0
WireConnection;490;3;385;0
WireConnection;490;4;100;0
WireConnection;305;0;303;0
WireConnection;321;0;324;0
WireConnection;491;0;506;0
WireConnection;491;3;400;0
WireConnection;491;4;21;0
WireConnection;507;0;509;0
WireConnection;309;0;305;0
WireConnection;316;0;321;0
WireConnection;429;0;491;0
WireConnection;428;0;490;0
WireConnection;308;0;44;0
WireConnection;423;0;415;0
WireConnection;310;0;309;0
WireConnection;307;0;308;0
WireConnection;315;0;316;0
WireConnection;422;0;414;0
WireConnection;508;0;507;0
WireConnection;0;0;307;0
WireConnection;0;1;310;0
WireConnection;0;2;508;0
WireConnection;0;3;432;0
WireConnection;0;4;433;0
WireConnection;0;5;315;0
ASEEND*/
//CHKSM=099A3B95D6F9095FD77F0F0872E278DBAB60B66B