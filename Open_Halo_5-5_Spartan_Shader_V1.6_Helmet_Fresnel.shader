// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Open Halo 5/5 Spartan Shader V1.6 Helmet Fresnel Visor"
{
	Properties
	{
		[Header(Open Halo 5 MP Spartan Shader v1.6 Fresnel)][Header(Main Image Inputs)][NoScaleOffset]_MainTex("Colour:", 2D) = "white" {}
		[NoScaleOffset]_ControlMap("Control Map:", 2D) = "white" {}
		[NoScaleOffset][Normal]_BumpMap("Normal Map:", 2D) = "bump" {}
		[Toggle]_Halo5NormalMap("Halo 5 Normal Map?", Float) = 0
		[Header(Ambient Occlusion Settings)][NoScaleOffset]_OcclusionMap("OcclusionMap:", 2D) = "white" {}
		[Toggle]_ManualAOMapInput("Switch to Manual AO Map Input?", Float) = 0
		_AOStrength("AO Strength", Range( 0 , 1)) = 1
		[Header(Metallic and Smoothness Settings)]_MetallicMultiplier("Metallic Multiplier", Range( 0 , 10)) = 1
		_SmoothnessMultiplier("Smoothness Multiplier ", Range( 0 , 1.25)) = 1
		_VisorMetallicMultiplier("Visor Metallic Multiplier", Range( 0 , 1.25)) = 1
		_VisorSmoothnessMultiplier("Visor Smoothness Multiplier", Range( 0 , 1.25)) = 1
		[Header(Colours)]_Color("Colour Red Channel", Color) = (1,0,0,0)
		_ColourGreenChannel("Colour Green Channel", Color) = (0.07799447,1,0,0)
		[Toggle]_ADDFakeZone3InverseofRGChannels("[ADD] Fake Zone 3? [Inverse of R&G Channels]", Float) = 1
		[HDR]_FakeZone3Colour("Fake Zone 3 Colour", Color) = (1,1,1,0)
		_ColourOfVisor("Colour Of Visor:", Color) = (1,0.9058824,0,0)
		_ColourOfVisorFresnel("Colour Of Visor Fresnel:", Color) = (1,0.09803922,0,1)
		[Header(Fresnel Settings)]_VisorFresnelBias("Visor Fresnel Bias", Range( 0 , 1)) = 0.1
		_VisorFresnelScale("Visor Fresnel Scale ", Range( 0 , 10)) = 2
		_VisorFresnelPower("Visor Fresnel Power", Range( 0 , 10)) = 1
		[Header(Emission)][NoScaleOffset]_EmissionMap("[ADD] Emission Texture:", 2D) = "black" {}
		_EmissionColor("Emission Color", Color) = (0,0,0,0)
		[Header(OPTIONAL Detail Textures)]_ADDRedDetailTexture("[ADD] Red Detail Texture:", 2D) = "white" {}
		_ADDGreenDetailTexture("[ADD] Green Detail Texture:", 2D) = "white" {}
		_ADDFakezone3DetailTexture("[ADD] Fake zone 3  Detail Texture:", 2D) = "white" {}
		_ADDVisorDetailAlbedo("[ADD] Visor Detail Albedo", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _BumpMap;
		uniform float _Halo5NormalMap;
		uniform sampler2D _MainTex;
		uniform sampler2D _ControlMap;
		uniform float4 _Color;
		uniform sampler2D _ADDRedDetailTexture;
		uniform float4 _ADDRedDetailTexture_ST;
		uniform float4 _ColourGreenChannel;
		uniform sampler2D _ADDGreenDetailTexture;
		uniform float4 _ADDGreenDetailTexture_ST;
		uniform float _ADDFakeZone3InverseofRGChannels;
		uniform float4 _FakeZone3Colour;
		uniform sampler2D _ADDFakezone3DetailTexture;
		uniform float4 _ADDFakezone3DetailTexture_ST;
		uniform sampler2D _ADDVisorDetailAlbedo;
		uniform float4 _ADDVisorDetailAlbedo_ST;
		uniform float4 _ColourOfVisor;
		uniform float _VisorFresnelBias;
		uniform float _VisorFresnelScale;
		uniform float _VisorFresnelPower;
		uniform float4 _ColourOfVisorFresnel;
		uniform float _ManualAOMapInput;
		uniform sampler2D _OcclusionMap;
		uniform float _AOStrength;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionColor;
		uniform float _MetallicMultiplier;
		uniform float _VisorMetallicMultiplier;
		uniform float _SmoothnessMultiplier;
		uniform float _VisorSmoothnessMultiplier;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_BumpMap7 = i.uv_texcoord;
			float3 tex2DNode7 = UnpackNormal( tex2D( _BumpMap, uv_BumpMap7 ) );
			float temp_output_695_0 = ( 1.0 * tex2DNode7.g );
			float2 appendResult706 = (float2(( ( tex2DNode7.r * 1.0 ) + 0.0 ) , (( _Halo5NormalMap )?( -temp_output_695_0 ):( temp_output_695_0 ))));
			float dotResult717 = dot( appendResult706 , appendResult706 );
			float temp_output_711_0 = sqrt( ( 1.0 - saturate( dotResult717 ) ) );
			float3 appendResult714 = (float3(appendResult706 , temp_output_711_0));
			o.Normal = appendResult714;
			float2 uv_MainTex6 = i.uv_texcoord;
			float4 Diffuse68 = tex2D( _MainTex, uv_MainTex6 );
			float2 uv_ControlMap1 = i.uv_texcoord;
			float4 tex2DNode1 = tex2D( _ControlMap, uv_ControlMap1 );
			float ControlR67 = tex2DNode1.r;
			float2 uv_ADDRedDetailTexture = i.uv_texcoord * _ADDRedDetailTexture_ST.xy + _ADDRedDetailTexture_ST.zw;
			float4 colorZone1Detail166 = tex2D( _ADDRedDetailTexture, uv_ADDRedDetailTexture );
			float ControlG73 = tex2DNode1.g;
			float2 uv_ADDGreenDetailTexture = i.uv_texcoord * _ADDGreenDetailTexture_ST.xy + _ADDGreenDetailTexture_ST.zw;
			float4 ColorZone2Detail168 = tex2D( _ADDGreenDetailTexture, uv_ADDGreenDetailTexture );
			float3 linearToGamma527 = LinearToGammaSpace( tex2DNode1.rgb );
			float3 break528 = linearToGamma527;
			float ControlRgamma535 = break528.x;
			float ControlGgamma536 = break528.y;
			float clampResult533 = clamp( ( ControlRgamma535 + ControlGgamma536 ) , 0.0 , 1.0 );
			float smoothstepResult534 = smoothstep( 0.0 , 1.0 , ( 1.0 - clampResult533 ));
			float4 temp_cast_2 = (smoothstepResult534).xxxx;
			float2 uv_ADDFakezone3DetailTexture = i.uv_texcoord * _ADDFakezone3DetailTexture_ST.xy + _ADDFakezone3DetailTexture_ST.zw;
			float4 ColorZone3Detail170 = tex2D( _ADDFakezone3DetailTexture, uv_ADDFakezone3DetailTexture );
			float clampResult568 = clamp( ( ( ( ControlR67 + ControlG73 ) - 1.2 ) * 100.0 ) , 0.0 , 1.0 );
			float VisorMask293 = clampResult568;
			float2 uv_ADDVisorDetailAlbedo = i.uv_texcoord * _ADDVisorDetailAlbedo_ST.xy + _ADDVisorDetailAlbedo_ST.zw;
			float4 VisorDetail331 = tex2D( _ADDVisorDetailAlbedo, uv_ADDVisorDetailAlbedo );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV722 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode722 = ( _VisorFresnelBias + _VisorFresnelScale * pow( 1.0 - fresnelNdotV722, _VisorFresnelPower ) );
			float saferPower712 = max( temp_output_711_0 , 0.0001 );
			float4 temp_cast_3 = (saturate( pow( saferPower712 , (4.0 + (0.0 - 0.0) * (25.0 - 4.0) / (1.0 - 0.0)) ) )).xxxx;
			float2 uv_OcclusionMap663 = i.uv_texcoord;
			float4 blendOpSrc682 = (( _ManualAOMapInput )?( tex2D( _OcclusionMap, uv_OcclusionMap663 ) ):( temp_cast_3 ));
			float4 blendOpDest682 = float4( 1,1,1,0 );
			float4 lerpBlendMode682 = lerp(blendOpDest682,( blendOpSrc682 * blendOpDest682 ),_AOStrength);
			float4 AmbientOcclusion734 = ( saturate( lerpBlendMode682 ));
			float4 VisorColour342 = ( ( ( ( VisorMask293 * ( VisorDetail331 * _ColourOfVisor ) ) * ( 1.0 - fresnelNode722 ) ) + ( ( fresnelNode722 * _ColourOfVisorFresnel ) * VisorMask293 ) ) * AmbientOcclusion734 );
			float4 blendOpSrc569 = Diffuse68;
			float4 blendOpDest569 = ( ( ( ( ( ControlR67 * ( _Color * colorZone1Detail166 ) ) + ( ControlG73 * ( _ColourGreenChannel * ColorZone2Detail168 ) ) ) + (( _ADDFakeZone3InverseofRGChannels )?( ( smoothstepResult534 * ( _FakeZone3Colour * ColorZone3Detail170 ) ) ):( temp_cast_2 )) ) * ( 1.0 - VisorMask293 ) ) + VisorColour342 );
			float4 temp_cast_4 = (( 0.5 + 0.5 )).xxxx;
			float4 blendOpSrc658 = ( saturate( ( blendOpSrc569 * blendOpDest569 ) ));
			float4 blendOpDest658 = temp_cast_4;
			float4 FiysColourMethod97 = ( saturate( ( blendOpSrc658 * blendOpDest658 ) ));
			o.Albedo = FiysColourMethod97.rgb;
			float2 uv_EmissionMap131 = i.uv_texcoord;
			o.Emission = ( tex2D( _EmissionMap, uv_EmissionMap131 ) * _EmissionColor ).rgb;
			float ControlBlue121 = tex2DNode1.b;
			float temp_output_381_0 = ( 1.0 - VisorMask293 );
			float ControlAlpha123 = tex2DNode1.a;
			float clampResult571 = clamp( ( ( ( ControlBlue121 * _MetallicMultiplier ) * temp_output_381_0 ) + ( ( ControlAlpha123 * VisorMask293 ) * _VisorMetallicMultiplier ) ) , 0.0 , 1.0 );
			float VarMetallic428 = clampResult571;
			o.Metallic = VarMetallic428;
			float clampResult570 = clamp( ( ( ( ControlAlpha123 * _SmoothnessMultiplier ) * temp_output_381_0 ) + ( ( ControlR67 * VisorMask293 ) * _VisorSmoothnessMultiplier ) ) , 0.0 , 1.0 );
			float VarSmoothness429 = clampResult570;
			o.Smoothness = VarSmoothness429;
			float4 clampResult580 = clamp( AmbientOcclusion734 , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Occlusion = clampResult580.r;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Standard"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18912
-1920;170;1920;1019;-1245.982;-2362.567;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;57;-496,-1536;Inherit;False;5743.567;2685.474;Colour Zones Metallics and Smoothness are all contained within the Control File. Red is Colour Zone 1, Green 2, Blue is the Halo 5 Metallic, And Alpha is the Halo 5 Smoothness;15;68;6;608;611;610;609;125;536;535;528;527;1;65;226;114;Diffuse & Control: Your Colouring Metallic and Smoothness Data;0,0.3379312,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;1;-481.0926,-1296;Inherit;True;Property;_ControlMap;Control Map:;1;1;[NoScaleOffset];Create;True;0;0;0;False;0;False;-1;None;751b59a050cf079429c9ad53f830f63a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;527;-480,-1088;Inherit;True;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;528;-480,-864;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.CommentaryNode;65;-465,-576;Inherit;False;4641.533;1704.518;Fiy's Colouring Method, with Point's Detail textures:        Using the Control, and Defuse, Let's Get some Armor Colours Going                                 More subtle then inital method, prevents blown colours.;33;128;569;380;660;348;213;346;211;344;596;347;343;595;543;542;545;599;205;82;590;81;594;64;592;591;488;489;606;604;116;605;607;126;Colouring: The most visable part of the Shader;0.3529412,0.8929005,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;126;207,640;Inherit;False;2261.492;419.854;Here the Red and Green Of the Control are Added, and then inverted, to create us the 3rd zone of the Armor's Colouring;11;96;94;533;534;526;85;208;207;174;120;661;THERE IS NO DEFAULT 3rd COLOUR ZONE;1,0.8482759,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;536;-160,-912;Float;True;ControlGgamma;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;535;-240,-1088;Float;True;ControlRgamma;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;208;527,688;Inherit;True;535;ControlRgamma;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;751,768;Inherit;True;536;ControlGgamma;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;226;832,-1472;Inherit;False;664.4159;777.4517;Registering Detail Albedos;8;331;332;170;171;168;167;166;165;Detail Albedos;0.9926471,0.175173,0.93627,1;0;0
Node;AmplifyShaderEditor.SamplerNode;171;864,-1088;Inherit;True;Property;_ADDFakezone3DetailTexture;[ADD] Fake zone 3  Detail Texture:;25;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;85;975,688;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;533;1183,688;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;170;1200,-1088;Float;True;ColorZone3Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;526;1439,688;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;684;-496,2256;Inherit;False;3434.633;1172.785;For Normal Maps, *NOTE* that the default format of Halo Normal Maps is for Direct X. Unity uses OPENGL Rendering, where the Green channel is used differently. INVERT GREEN CHANNEL TO FIX IT FOR UNITY!;13;716;714;706;11;662;702;699;696;695;719;691;718;7;Normals (Thank the PDevice for the good stuff);0,0.6689658,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;120;239,688;Float;False;Property;_FakeZone3Colour;Fake Zone 3 Colour;14;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;174;224,864;Inherit;True;170;ColorZone3Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;534;1615,688;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;7;-464,2352;Inherit;True;Property;_BumpMap;Normal Map:;2;2;[NoScaleOffset];[Normal];Create;False;0;0;0;False;0;False;-1;None;a4bf58953a27e894e8e6a93058953e05;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;661;576,944;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;718;-144,2560;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;1887,832;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;125;240,-1440;Inherit;False;528.7554;750.0869;Spliting the Control into channels as intended for use;6;67;280;279;121;123;73;The Control Split;1,1,1,1;0;0
Node;AmplifyShaderEditor.ToggleSwitchNode;96;2143,736;Float;True;Property;_ADDFakeZone3InverseofRGChannels;[ADD] Fake Zone 3? [Inverse of R&G Channels];13;0;Create;True;0;0;0;False;0;False;1;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;719;-144,2560;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;691;-80,2448;Inherit;False;Constant;_NormalMapIntensity;Normal Map Intensity;8;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;544,-1152;Float;True;ControlG;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;67;352,-1392;Float;True;ControlR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;294;-496,1280;Inherit;False;3774.213;877.3869;Fiy's Visor Isolation Method, Crude, but it works;24;735;342;733;727;726;728;340;725;724;723;338;722;339;336;293;568;567;566;560;257;256;736;737;738;Visor;1,0.6196079,0,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;256;-464,1360;Inherit;True;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;257;-464,1552;Inherit;True;73;ControlG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;607;2479,768;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;695;192,2512;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;696;432,2640;Inherit;False;158;152;Y;1;701;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;702;192,2384;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;701;448,2720;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;560;-128,1424;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;605;2495,768;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;699;432,2336;Inherit;False;158;152;X;1;703;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;662;1072,2624;Inherit;False;1788.471;753.6282;Maps for lighting on the Armor;7;682;243;708;715;663;705;734;AO with an Auto Approx Calculation ;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.WireNode;604;2495,752;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;11;720,2576;Float;True;Property;_Halo5NormalMap;Halo 5 Normal Map?;3;0;Create;True;0;0;0;False;0;False;0;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;703;464,2384;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;566;96,1424;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;1.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;567;336,1424;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;100;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;606;2495,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;705;1120,2688;Inherit;False;959.0999;342.6997;Fake AO Calculation;7;717;713;712;711;710;709;707;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;706;976,2384;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;165;864,-1440;Inherit;True;Property;_ADDRedDetailTexture;[ADD] Red Detail Texture:;23;1;[Header];Create;True;1;OPTIONAL Detail Textures;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DotProductOpNode;717;1152,2752;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;568;656,1424;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;167;864,-1264;Inherit;True;Property;_ADDGreenDetailTexture;[ADD] Green Detail Texture:;24;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;114;1520,-1472;Inherit;False;3721.344;882.909;Hidden in the Control, and tossed by most, the Blue channel is actually the Metallic, and Alpha is Smoothness;35;428;429;393;650;645;652;651;655;381;654;653;649;648;647;646;388;636;400;385;384;398;389;383;397;399;396;394;21;100;124;20;122;99;506;644;Metallic And Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;489;2479,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;293;928,1424;Float;True;VisorMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;644;2226.527,-1195.864;Inherit;False;300.4015;248.2181;Check "Visor" Section for Source;1;382;;1,0.6196079,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;168;1200,-1264;Float;True;ColorZone2Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;707;1280,2752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;166;1200,-1440;Float;True;colorZone1Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;116;-449,-528;Inherit;False;618.8059;1581.453;Grabbing Data From Split & User Etc;9;172;79;119;173;66;118;69;209;210;;1,0,0,1;0;0
Node;AmplifyShaderEditor.WireNode;488;863,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;173;-433,656;Inherit;True;168;ColorZone2Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;119;-433,480;Float;False;Property;_ColourGreenChannel;Colour Green Channel;12;0;Create;True;0;0;0;False;0;False;0.07799447,1,0,0;0.07799403,1,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;118;-433,-96;Float;False;Property;_Color;Colour Red Channel;11;1;[Header];Create;False;1;Colours;0;0;False;0;False;1,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;591;847,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;609;-208,-1168;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;382;2240,-1152;Inherit;True;293;VisorMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;709;1424,2752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;172;-433,80;Inherit;True;166;colorZone1Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;592;847,560;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;710;1504,2864;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;4;False;4;FLOAT;25;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;66;-433,-288;Inherit;True;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;209;-128,-96;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;210;-112,480;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;649;2944,-1120;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SqrtOpNode;711;1584,2752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-433,272;Inherit;True;73;ControlG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;610;-208,-1168;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;332;864,-912;Inherit;True;Property;_ADDVisorDetailAlbedo;[ADD] Visor Detail Albedo;26;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;191,-144;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;207,352;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;712;1728,2784;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;594;847,320;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;611;176,-1056;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;647;2944,-1120;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;713;1872,2784;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;331;1200,-912;Float;True;VisorDetail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;82;447,240;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;729;1136,2128;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;590;847,304;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;608;176,-1056;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;645;2976,-944;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;663;1136,3088;Inherit;True;Property;_OcclusionMap;OcclusionMap:;4;2;[Header];[NoScaleOffset];Create;False;1;Ambient Occlusion Settings;0;0;False;0;False;-1;None;8ff6f5551ffe2744ba30023811c2e0f1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;336;1184,1488;Inherit;True;331;VisorDetail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;279;256,-880;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;738;1312,1856;Inherit;False;Property;_VisorFresnelBias;Visor Fresnel Bias;17;1;[Header];Create;True;1;Fresnel Settings;0;0;False;0;False;0.1;0.33;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RelayNode;715;2099.5,2786.6;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;732;1136,2128;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;648;2944,-1120;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;205;895,240;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;737;1312,2016;Inherit;False;Property;_VisorFresnelPower;Visor Fresnel Power;19;0;Create;True;0;0;0;False;0;False;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;650;2976,-944;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;733;1312,1936;Inherit;False;Property;_VisorFresnelScale;Visor Fresnel Scale ;18;0;Create;True;0;0;0;False;0;False;2;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;339;1184,1680;Float;False;Property;_ColourOfVisor;Colour Of Visor:;15;0;Create;True;0;0;0;False;0;False;1,0.9058824,0,0;1,0.7038869,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;280;272,-880;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;731;2304,2128;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;243;2016,3072;Float;True;Property;_ManualAOMapInput;Switch to Manual AO Map Input?;5;0;Create;False;0;0;0;False;0;False;0;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;338;1408,1568;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;651;2976,-752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;599;1087,224;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FresnelNode;722;1744,1712;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0.1;False;2;FLOAT;2;False;3;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;723;1744,1952;Float;False;Property;_ColourOfVisorFresnel;Colour Of Visor Fresnel:;16;0;Create;True;0;0;0;False;0;False;1,0.09803922,0,1;1,0.09803919,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;646;2944,-1120;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;708;2032,3296;Inherit;False;Property;_AOStrength;AO Strength;6;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;652;2976,-752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;682;2352,3072;Inherit;False;Multiply;True;3;0;COLOR;0,0,0,0;False;1;COLOR;1,1,1,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;725;2112,1808;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;730;2304,2128;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;724;2112,1504;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;121;288,-1088;Float;True;ControlBlue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;123;544,-912;Float;True;ControlAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;545;1087,208;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;654;3200,-1200;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;340;1728,1424;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;99;1742.995,-1328;Float;False;Property;_MetallicMultiplier;Metallic Multiplier;7;1;[Header];Create;True;1;Metallic and Smoothness Settings;0;0;False;0;False;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;124;1536,-944;Inherit;True;123;ControlAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;655;3200,-752;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;394;2944,-1264;Inherit;False;123;ControlAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;122;1536,-1392;Inherit;True;121;ControlBlue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-480,-1488;Inherit;True;Property;_MainTex;Colour:;0;2;[Header];[NoScaleOffset];Create;False;2;Open Halo 5 MP Spartan Shader v1.6 Fresnel;Main Image Inputs;0;0;False;0;False;-1;None;a78c64421a2a1bb489d933b3dc6528ae;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;20;1744,-864;Float;False;Property;_SmoothnessMultiplier;Smoothness Multiplier ;8;0;Create;True;0;0;0;False;0;False;1;1;0;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;726;2320,1424;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;734;2640,3072;Float;True;AmbientOcclusion;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;728;2384,1920;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;542;991,32;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;396;3040,-816;Inherit;False;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;653;3200,-1200;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;735;2672,1680;Inherit;True;734;AmbientOcclusion;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;388;3152,-1088;Float;False;Property;_VisorMetallicMultiplier;Visor Metallic Multiplier;9;0;Create;True;0;0;0;False;0;False;1;1;0;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;397;3248,-816;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;2016,-944;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;399;3120,-672;Float;False;Property;_VisorSmoothnessMultiplier;Visor Smoothness Multiplier;10;0;Create;True;0;0;0;False;0;False;1;1;0;1.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;2016,-1392;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;381;2560,-1184;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;727;2576,1408;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;543;991,16;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;393;3248,-1264;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;16,-1488;Float;False;Diffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;-433,-496;Inherit;True;68;Diffuse;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;384;2736,-880;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;398;3456,-816;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;389;3456,-1264;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;343;607,-240;Inherit;True;293;VisorMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;595;991,-224;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;736;2924.323,1431.216;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;383;2736,-1328;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;385;3600,-1328;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;180;-496,3552;Inherit;False;2956.391;523.158;Maps for lighting on the Armor;5;509;510;144;143;131;Emmission;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;636;4400,-1408;Inherit;False;604;697;These Clamps are here to Prevent Artifacting, due to Blown out Values;2;570;571;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;211;1567,-448;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;596;1007,-224;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;342;3107,1418;Float;True;VisorColour;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;347;799,-240;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;400;3600,-880;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;660;2560,64;Inherit;False;889.7329;377.3918;These Ensure players don't look flat when shaders are hidden;4;657;656;658;659;Fallback Properties;1,0,0,1;0;0
Node;AmplifyShaderEditor.WireNode;213;1583,-448;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;143;-160,3776;Float;False;Property;_EmissionColor;Emission Color;22;0;Create;False;0;0;0;False;0;False;0,0,0,0;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;320;4144,1536;Inherit;False;1181.235;1727.258;Where it's all coming Together;15;0;508;572;315;310;576;589;573;579;309;507;588;580;637;638;OUTPUT;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;344;1071,-48;Inherit;False;342;VisorColour;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;131;-464,3616;Inherit;True;Property;_EmissionMap;[ADD] Emission Texture:;21;2;[Header];[NoScaleOffset];Create;False;1;Emission;0;0;False;0;False;-1;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;570;4560,-944;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;346;1071,-256;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;571;4560,-1328;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;144;-64,3648;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;429;5024,-944;Float;False;VarSmoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;380;1936,160;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;714;1840,2384;Inherit;False;FLOAT3;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;637;4183.635,2087.84;Inherit;False;485.7451;455.2663;Look In "Metallic And Smoothness" Section for Source;4;574;575;432;433;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;348;1295,-176;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;657;2688,320;Float;False;Constant;_Glossiness;Glossiness;19;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;428;5040,-1328;Float;False;VarMetallic;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;656;2688,224;Float;False;Constant;_Metallic;Metallic;19;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;510;2384,3696;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;569;1968,128;Inherit;True;Multiply;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;433;4216.194,2348.052;Inherit;True;429;VarSmoothness;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;659;2958,268;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;580;4464,2592;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;432;4216.194,2140.052;Inherit;True;428;VarMetallic;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RelayNode;716;2208,2320;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;574;4424.194,2380.052;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;128;3552,80;Inherit;False;478.1267;151.4682;Export Finished Colouring For Use Elsewhere in the Shader;1;97;;0,1,0.2551723,1;0;0
Node;AmplifyShaderEditor.RelayNode;720;4160,1888;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;509;2384,3696;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;575;4392.194,2172.052;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;658;3104,128;Inherit;True;Multiply;True;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;588;4672,2624;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;507;4288,1952;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;573;4744.194,2140.052;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;589;4672,2608;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;638;4176,1680;Inherit;False;303.8372;133.5029;Check Colouring Section for Source;1;98;;0,1,0.2551723,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;97;3680,128;Float;False;FiysColourMethod;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;309;4288,1920;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;579;4744.194,2108.052;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;572;4744.194,2140.052;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;98;4224,1728;Inherit;False;97;FiysColourMethod;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;310;4288,1920;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;315;4880,2032;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;506;3600,-1104;Float;False;Property;_IsthisaHelmet;[Is this a Helmet?];20;0;Create;True;0;0;0;False;0;False;1;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;576;4744.194,2108.052;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;508;4288,1952;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;4936.194,1868.052;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Open Halo 5/5 Spartan Shader V1.6 Helmet Fresnel Visor;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;Standard;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;129;-480,-1968;Inherit;False;100;100;...;0;Created By PointDevice And FiyCsf For Use With Halo 5 Spartan Models;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;253;-496,-1760;Inherit;False;361.9877;100;GNU LESSER GENERAL PUBLIC LICENSE                        Version 2.1, February 1999   Copyright (C) 1991, 1999 Free Software Foundation, Inc.  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  Everyone is permitted to copy and distribute verbatim copies  of this license document, but changing it is not allowed.  (This is the first released version of the Lesser GPL.  It also counts  as the successor of the GNU Library Public License, version 2, hence  the version number 2.1.)                              Preamble    The licenses for most software are designed to take away your freedom to share and change it.  By contrast, the GNU General Public Licenses are intended to guarantee your freedom to share and change free software--to make sure the software is free for all its users.    This license, the Lesser General Public License, applies to some specially designated software packages--typically libraries--of the Free Software Foundation and other authors who decide to use it.  You can use it too, but we suggest you first think carefully about whether this license or the ordinary General Public License is the better strategy to use in any particular case, based on the explanations below.    When we speak of free software, we are referring to freedom of use, not price.  Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for this service if you wish)  that you receive source code or can get it if you want it  that you can change the software and use pieces of it in new free programs  and that you are informed that you can do these things.    To protect your rights, we need to make restrictions that forbid distributors to deny you these rights or to ask you to surrender these rights.  These restrictions translate to certain responsibilities for you if you distribute copies of the library or if you modify it.    For example, if you distribute copies of the library, whether gratis or for a fee, you must give the recipients all the rights that we gave you.  You must make sure that they, too, receive or can get the source code.  If you link other code with the library, you must provide complete object files to the recipients, so that they can relink them with the library after making changes to the library and recompiling it.  And you must show them these terms so they know their rights.    We protect your rights with a two-step method: (1) we copyright the library, and (2) we offer you this license, which gives you legal permission to copy, distribute and/or modify the library.    To protect each distributor, we want to make it very clear that there is no warranty for the free library.  Also, if the library is modified by someone else and passed on, the recipients should know that what they have is not the original version, so that the original author's reputation will not be affected by problems that might be introduced by others.    Finally, software patents pose a constant threat to the existence of any free program.  We wish to make sure that a company cannot effectively restrict the users of a free program by obtaining a restrictive license from a patent holder.  Therefore, we insist that any patent license obtained for a version of the library must be consistent with the full freedom of use specified in this license.    Most GNU software, including some libraries, is covered by the ordinary GNU General Public License.  This license, the GNU Lesser General Public License, applies to certain designated libraries, and is quite different from the ordinary General Public License.  We use this license for certain libraries in order to permit linking those libraries into non-free programs.    When a program is linked with a library, whether statically or using a shared library, the combination of the two is legally speaking a combined work, a derivative of the original library.  The ordinary General Public License therefore permits such linking only if the entire combination fits its criteria of freedom.  The Lesser General Public License permits more lax criteria for linking other code with the library.    We call this license the "Lesser" General Public License because it does Less to protect the user's freedom than the ordinary General Public License.  It also provides other free software developers Less of an advantage over competing non-free programs.  These disadvantages are the reason we use the ordinary General Public License for many libraries.  However, the Lesser license provides advantages in certain special circumstances.    For example, on rare occasions, there may be a special need to encourage the widest possible use of a certain library, so that it becomes a de-facto standard.  To achieve this, non-free programs must be allowed to use the library.  A more frequent case is that a free library does the same job as widely used non-free libraries.  In this case, there is little to gain by limiting the free library to free software only, so we use the Lesser General Public License.    In other cases, permission to use a particular library in non-free programs enables a greater number of people to use a large body of free software.  For example, permission to use the GNU C Library in non-free programs enables many more people to use the whole GNU operating system, as well as its variant, the GNU/Linux operating system.    Although the Lesser General Public License is Less protective of the users' freedom, it does ensure that the user of a program that is linked with the Library has the freedom and the wherewithal to run that program using a modified version of the Library.    The precise terms and conditions for copying, distribution and modification follow.  Pay close attention to the difference between a "work based on the library" and a "work that uses the library".  The former contains code derived from the library, whereas the latter must be combined with the library in order to run.                    GNU LESSER GENERAL PUBLIC LICENSE    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION    0. This License Agreement applies to any software library or other program which contains a notice placed by the copyright holder or other authorized party saying it may be distributed under the terms of this Lesser General Public License (also called "this License"). Each licensee is addressed as "you".    A "library" means a collection of software functions and/or data prepared so as to be conveniently linked with application programs (which use some of those functions and data) to form executables.    The "Library", below, refers to any such software library or work which has been distributed under these terms.  A "work based on the Library" means either the Library or any derivative work under copyright law: that is to say, a work containing the Library or a portion of it, either verbatim or with modifications and/or translated straightforwardly into another language.  (Hereinafter, translation is included without limitation in the term "modification".)    "Source code" for a work means the preferred form of the work for making modifications to it.  For a library, complete source code means all the source code for all modules it contains, plus any associated interface definition files, plus the scripts used to control compilation and installation of the library.    Activities other than copying, distribution and modification are not covered by this License  they are outside its scope.  The act of running a program using the Library is not restricted, and output from such a program is covered only if its contents constitute a work based on the Library (independent of the use of the Library in a tool for writing it).  Whether that is true depends on what the Library does and what the program that uses the Library does.    1. You may copy and distribute verbatim copies of the Library's complete source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice and disclaimer of warranty  keep intact all the notices that refer to this License and to the absence of any warranty  and distribute a copy of this License along with the Library.    You may charge a fee for the physical act of transferring a copy, and you may at your option offer warranty protection in exchange for a fee.    2. You may modify your copy or copies of the Library or any portion of it, thus forming a work based on the Library, and copy and distribute such modifications or work under the terms of Section 1 above, provided that you also meet all of these conditions:      a) The modified work must itself be a software library.      b) You must cause the files modified to carry prominent notices     stating that you changed the files and the date of any change.      c) You must cause the whole of the work to be licensed at no     charge to all third parties under the terms of this License.      d) If a facility in the modified Library refers to a function or a     table of data to be supplied by an application program that uses     the facility, other than as an argument passed when the facility     is invoked, then you must make a good faith effort to ensure that,     in the event an application does not supply such function or     table, the facility still operates, and performs whatever part of     its purpose remains meaningful.      (For example, a function in a library to compute square roots has     a purpose that is entirely well-defined independent of the     application.  Therefore, Subsection 2d requires that any     application-supplied function or table used by this function must     be optional: if the application does not supply it, the square     root function must still compute square roots.)  These requirements apply to the modified work as a whole.  If identifiable sections of that work are not derived from the Library, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works.  But when you distribute the same sections as part of a whole which is a work based on the Library, the distribution of the whole must be on the terms of this License, whose permissions for other licensees extend to the entire whole, and thus to each and every part regardless of who wrote it.  Thus, it is not the intent of this section to claim rights or contest your rights to work written entirely by you  rather, the intent is to exercise the right to control the distribution of derivative or collective works based on the Library.  In addition, mere aggregation of another work not based on the Library with the Library (or with a work based on the Library) on a volume of a storage or distribution medium does not bring the other work under the scope of this License.    3. You may opt to apply the terms of the ordinary GNU General Public License instead of this License to a given copy of the Library.  To do this, you must alter all the notices that refer to this License, so that they refer to the ordinary GNU General Public License, version 2, instead of to this License.  (If a newer version than version 2 of the ordinary GNU General Public License has appeared, then you can specify that version instead if you wish.)  Do not make any other change in these notices.    Once this change is made in a given copy, it is irreversible for that copy, so the ordinary GNU General Public License applies to all subsequent copies and derivative works made from that copy.    This option is useful when you wish to copy part of the code of the Library into a program that is not a library.    4. You may copy and distribute the Library (or a portion or derivative of it, under Section 2) in object code or executable form under the terms of Sections 1 and 2 above provided that you accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange.    If distribution of object code is made by offering access to copy from a designated place, then offering equivalent access to copy the source code from the same place satisfies the requirement to distribute the source code, even though third parties are not compelled to copy the source along with the object code.    5. A program that contains no derivative of any portion of the Library, but is designed to work with the Library by being compiled or linked with it, is called a "work that uses the Library".  Such a work, in isolation, is not a derivative work of the Library, and therefore falls outside the scope of this License.    However, linking a "work that uses the Library" with the Library creates an executable that is a derivative of the Library (because it contains portions of the Library), rather than a "work that uses the library".  The executable is therefore covered by this License. Section 6 states terms for distribution of such executables.    When a "work that uses the Library" uses material from a header file that is part of the Library, the object code for the work may be a derivative work of the Library even though the source code is not. Whether this is true is especially significant if the work can be linked without the Library, or if the work is itself a library.  The threshold for this to be true is not precisely defined by law.    If such an object file uses only numerical parameters, data structure layouts and accessors, and small macros and small inline functions (ten lines or less in length), then the use of the object file is unrestricted, regardless of whether it is legally a derivative work.  (Executables containing this object code plus portions of the Library will still fall under Section 6.)    Otherwise, if the work is a derivative of the Library, you may distribute the object code for the work under the terms of Section 6. Any executables containing that work also fall under Section 6, whether or not they are linked directly with the Library itself.    6. As an exception to the Sections above, you may also combine or link a "work that uses the Library" with the Library to produce a work containing portions of the Library, and distribute that work under terms of your choice, provided that the terms permit modification of the work for the customer's own use and reverse engineering for debugging such modifications.    You must give prominent notice with each copy of the work that the Library is used in it and that the Library and its use are covered by this License.  You must supply a copy of this License.  If the work during execution displays copyright notices, you must include the copyright notice for the Library among them, as well as a reference directing the user to the copy of this License.  Also, you must do one of these things:      a) Accompany the work with the complete corresponding     machine-readable source code for the Library including whatever     changes were used in the work (which must be distributed under     Sections 1 and 2 above)  and, if the work is an executable linked     with the Library, with the complete machine-readable "work that     uses the Library", as object code and/or source code, so that the     user can modify the Library and then relink to produce a modified     executable containing the modified Library.  (It is understood     that the user who changes the contents of definitions files in the     Library will not necessarily be able to recompile the application     to use the modified definitions.)      b) Use a suitable shared library mechanism for linking with the     Library.  A suitable mechanism is one that (1) uses at run time a     copy of the library already present on the user's computer system,     rather than copying library functions into the executable, and (2)     will operate properly with a modified version of the library, if     the user installs one, as long as the modified version is     interface-compatible with the version that the work was made with.      c) Accompany the work with a written offer, valid for at     least three years, to give the same user the materials     specified in Subsection 6a, above, for a charge no more     than the cost of performing this distribution.      d) If distribution of the work is made by offering access to copy     from a designated place, offer equivalent access to copy the above     specified materials from the same place.      e) Verify that the user has already received a copy of these     materials or that you have already sent this user a copy.    For an executable, the required form of the "work that uses the Library" must include any data and utility programs needed for reproducing the executable from it.  However, as a special exception, the materials to be distributed need not include anything that is normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which the executable runs, unless that component itself accompanies the executable.    It may happen that this requirement contradicts the license restrictions of other proprietary libraries that do not normally accompany the operating system.  Such a contradiction means you cannot use both them and the Library together in an executable that you distribute.    7. You may place library facilities that are a work based on the Library side-by-side in a single library together with other library facilities not covered by this License, and distribute such a combined library, provided that the separate distribution of the work based on the Library and of the other library facilities is otherwise permitted, and provided that you do these two things:      a) Accompany the combined library with a copy of the same work     based on the Library, uncombined with any other library     facilities.  This must be distributed under the terms of the     Sections above.      b) Give prominent notice with the combined library of the fact     that part of it is a work based on the Library, and explaining     where to find the accompanying uncombined form of the same work.    8. You may not copy, modify, sublicense, link with, or distribute the Library except as expressly provided under this License.  Any attempt otherwise to copy, modify, sublicense, link with, or distribute the Library is void, and will automatically terminate your rights under this License.  However, parties who have received copies, or rights, from you under this License will not have their licenses terminated so long as such parties remain in full compliance.    9. You are not required to accept this License, since you have not signed it.  However, nothing else grants you permission to modify or distribute the Library or its derivative works.  These actions are prohibited by law if you do not accept this License.  Therefore, by modifying or distributing the Library (or any work based on the Library), you indicate your acceptance of this License to do so, and all its terms and conditions for copying, distributing or modifying the Library or works based on it.    10. Each time you redistribute the Library (or any work based on the Library), the recipient automatically receives a license from the original licensor to copy, distribute, link with or modify the Library subject to these terms and conditions.  You may not impose any further restrictions on the recipients' exercise of the rights granted herein. You are not responsible for enforcing compliance by third parties with this License.    11. If, as a consequence of a court judgment or allegation of patent infringement or for any other reason (not limited to patent issues), conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License.  If you cannot distribute so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not distribute the Library at all.  For example, if a patent license would not permit royalty-free redistribution of the Library by all those who receive copies directly or indirectly through you, then the only way you could satisfy both it and this License would be to refrain entirely from distribution of the Library.  If any portion of this section is held invalid or unenforceable under any particular circumstance, the balance of the section is intended to apply, and the section as a whole is intended to apply in other circumstances.  It is not the purpose of this section to induce you to infringe any patents or other property right claims or to contest validity of any such claims  this section has the sole purpose of protecting the integrity of the free software distribution system which is implemented by public license practices.  Many people have made generous contributions to the wide range of software distributed through that system in reliance on consistent application of that system  it is up to the author/donor to decide if he or she is willing to distribute software through any other system and a licensee cannot impose that choice.  This section is intended to make thoroughly clear what is believed to be a consequence of the rest of this License.    12. If the distribution and/or use of the Library is restricted in certain countries either by patents or by copyrighted interfaces, the original copyright holder who places the Library under this License may add an explicit geographical distribution limitation excluding those countries, so that distribution is permitted only in or among countries not thus excluded.  In such case, this License incorporates the limitation as if written in the body of this License.    13. The Free Software Foundation may publish revised and/or new versions of the Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.  Each version is given a distinguishing version number.  If the Library specifies a version number of this License which applies to it and "any later version", you have the option of following the terms and conditions either of that version or of any later version published by the Free Software Foundation.  If the Library does not specify a license version number, you may choose any version ever published by the Free Software Foundation.    14. If you wish to incorporate parts of the Library into other free programs whose distribution conditions are incompatible with these, write to the author to ask for permission.  For software which is copyrighted by the Free Software Foundation, write to the Free Software Foundation  we sometimes make exceptions for this.  Our decision will be guided by the two goals of preserving the free status of all derivatives of our free software and of promoting the sharing and reuse of software generally.                              NO WARRANTY    15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.    16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.                       END OF TERMS AND CONDITIONS             How to Apply These Terms to Your New Libraries    If you develop a new library, and you want it to be of the greatest possible use to the public, we recommend making it free software that everyone can redistribute and change.  You can do so by permitting redistribution under these terms (or, alternatively, under the terms of the ordinary General Public License).    To apply these terms, attach the following notices to the library.  It is safest to attach them to the start of each source file to most effectively convey the exclusion of warranty  and each file should have at least the "copyright" line and a pointer to where the full notice is found.      shaders     Copyright (C) 2019 John W      This library is free software  you can redistribute it and/or     modify it under the terms of the GNU Lesser General Public     License as published by the Free Software Foundation  either     version 2.1 of the License, or (at your option) any later version.      This library is distributed in the hope that it will be useful,     but WITHOUT ANY WARRANTY  without even the implied warranty of     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     Lesser General Public License for more details.      You should have received a copy of the GNU Lesser General Public     License along with this library  if not, write to the Free Software     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301     USA  Also add information on how to contact you by electronic and paper mail.  You should also get your employer (if you work as a programmer) or your school, if any, to sign a "copyright disclaimer" for the library, if necessary.  Here is a sample  alter the names:    Yoyodyne, Inc., hereby disclaims all copyright interest in the   library `Frob' (a library for tweaking knobs) written by James Random   Hacker.    {signature of Ty Coon}, 1 April 1990   Ty Coon, President of Vice  That's all there is to it! ;0;This Shader is Licensed under the LGPL License Please read the associated Text File.;1,1,1,1;0;0
WireConnection;527;0;1;0
WireConnection;528;0;527;0
WireConnection;536;0;528;1
WireConnection;535;0;528;0
WireConnection;85;0;208;0
WireConnection;85;1;207;0
WireConnection;533;0;85;0
WireConnection;170;0;171;0
WireConnection;526;0;533;0
WireConnection;534;0;526;0
WireConnection;661;0;120;0
WireConnection;661;1;174;0
WireConnection;718;0;7;2
WireConnection;94;0;534;0
WireConnection;94;1;661;0
WireConnection;96;0;534;0
WireConnection;96;1;94;0
WireConnection;719;0;718;0
WireConnection;73;0;1;2
WireConnection;67;0;1;1
WireConnection;607;0;96;0
WireConnection;695;0;691;0
WireConnection;695;1;719;0
WireConnection;702;0;7;1
WireConnection;702;1;691;0
WireConnection;701;0;695;0
WireConnection;560;0;256;0
WireConnection;560;1;257;0
WireConnection;605;0;607;0
WireConnection;604;0;605;0
WireConnection;11;0;695;0
WireConnection;11;1;701;0
WireConnection;703;0;702;0
WireConnection;566;0;560;0
WireConnection;567;0;566;0
WireConnection;606;0;604;0
WireConnection;706;0;703;0
WireConnection;706;1;11;0
WireConnection;717;0;706;0
WireConnection;717;1;706;0
WireConnection;568;0;567;0
WireConnection;489;0;606;0
WireConnection;293;0;568;0
WireConnection;168;0;167;0
WireConnection;707;0;717;0
WireConnection;166;0;165;0
WireConnection;488;0;489;0
WireConnection;591;0;488;0
WireConnection;609;0;1;4
WireConnection;709;0;707;0
WireConnection;592;0;591;0
WireConnection;209;0;118;0
WireConnection;209;1;172;0
WireConnection;210;0;119;0
WireConnection;210;1;173;0
WireConnection;649;0;382;0
WireConnection;711;0;709;0
WireConnection;610;0;609;0
WireConnection;64;0;66;0
WireConnection;64;1;209;0
WireConnection;81;0;79;0
WireConnection;81;1;210;0
WireConnection;712;0;711;0
WireConnection;712;1;710;0
WireConnection;594;0;592;0
WireConnection;611;0;610;0
WireConnection;647;0;649;0
WireConnection;713;0;712;0
WireConnection;331;0;332;0
WireConnection;82;0;64;0
WireConnection;82;1;81;0
WireConnection;729;0;293;0
WireConnection;590;0;594;0
WireConnection;608;0;611;0
WireConnection;645;0;647;0
WireConnection;279;0;608;0
WireConnection;715;0;713;0
WireConnection;732;0;729;0
WireConnection;648;0;382;0
WireConnection;205;0;82;0
WireConnection;205;1;590;0
WireConnection;650;0;645;0
WireConnection;280;0;279;0
WireConnection;731;0;732;0
WireConnection;243;0;715;0
WireConnection;243;1;663;0
WireConnection;338;0;336;0
WireConnection;338;1;339;0
WireConnection;651;0;650;0
WireConnection;599;0;205;0
WireConnection;722;1;738;0
WireConnection;722;2;733;0
WireConnection;722;3;737;0
WireConnection;646;0;648;0
WireConnection;652;0;651;0
WireConnection;682;0;243;0
WireConnection;682;2;708;0
WireConnection;725;0;722;0
WireConnection;725;1;723;0
WireConnection;730;0;731;0
WireConnection;724;0;722;0
WireConnection;121;0;1;3
WireConnection;123;0;280;0
WireConnection;545;0;599;0
WireConnection;654;0;646;0
WireConnection;340;0;293;0
WireConnection;340;1;338;0
WireConnection;655;0;652;0
WireConnection;726;0;340;0
WireConnection;726;1;724;0
WireConnection;734;0;682;0
WireConnection;728;0;725;0
WireConnection;728;1;730;0
WireConnection;542;0;545;0
WireConnection;653;0;654;0
WireConnection;397;0;396;0
WireConnection;397;1;655;0
WireConnection;21;0;124;0
WireConnection;21;1;20;0
WireConnection;100;0;122;0
WireConnection;100;1;99;0
WireConnection;381;0;382;0
WireConnection;727;0;726;0
WireConnection;727;1;728;0
WireConnection;543;0;542;0
WireConnection;393;0;394;0
WireConnection;393;1;653;0
WireConnection;68;0;6;0
WireConnection;384;0;21;0
WireConnection;384;1;381;0
WireConnection;398;0;397;0
WireConnection;398;1;399;0
WireConnection;389;0;393;0
WireConnection;389;1;388;0
WireConnection;595;0;543;0
WireConnection;736;0;727;0
WireConnection;736;1;735;0
WireConnection;383;0;100;0
WireConnection;383;1;381;0
WireConnection;385;0;383;0
WireConnection;385;1;389;0
WireConnection;211;0;69;0
WireConnection;596;0;595;0
WireConnection;342;0;736;0
WireConnection;347;0;343;0
WireConnection;400;0;384;0
WireConnection;400;1;398;0
WireConnection;213;0;211;0
WireConnection;570;0;400;0
WireConnection;346;0;596;0
WireConnection;346;1;347;0
WireConnection;571;0;385;0
WireConnection;144;0;131;0
WireConnection;144;1;143;0
WireConnection;429;0;570;0
WireConnection;380;0;213;0
WireConnection;714;0;706;0
WireConnection;714;2;711;0
WireConnection;348;0;346;0
WireConnection;348;1;344;0
WireConnection;428;0;571;0
WireConnection;510;0;144;0
WireConnection;569;0;380;0
WireConnection;569;1;348;0
WireConnection;659;0;656;0
WireConnection;659;1;657;0
WireConnection;580;0;734;0
WireConnection;716;0;714;0
WireConnection;574;0;433;0
WireConnection;720;0;716;0
WireConnection;509;0;510;0
WireConnection;575;0;432;0
WireConnection;658;0;569;0
WireConnection;658;1;659;0
WireConnection;588;0;580;0
WireConnection;507;0;509;0
WireConnection;573;0;574;0
WireConnection;589;0;588;0
WireConnection;97;0;658;0
WireConnection;309;0;720;0
WireConnection;579;0;575;0
WireConnection;572;0;573;0
WireConnection;310;0;309;0
WireConnection;315;0;589;0
WireConnection;576;0;579;0
WireConnection;508;0;507;0
WireConnection;0;0;98;0
WireConnection;0;1;310;0
WireConnection;0;2;508;0
WireConnection;0;3;576;0
WireConnection;0;4;572;0
WireConnection;0;5;315;0
ASEEND*/
//CHKSM=51B26D035A327DFA1E1FCD63A1281FFB6197D9CD